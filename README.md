#Magento Module XML Debugger

###Log entire XML path which is built by Magento
####Makes it easy to see what exactly is being loaded during page builds

Copy this file to your Magento base directory
`$cp -r app /path/to/magento/www`

refresh the page you want to debug and open the log file at `var/log/layout.log`


